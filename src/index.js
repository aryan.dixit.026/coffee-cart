import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import About from "./components/About";
import { createTheme, ThemeProvider } from "@mui/material";
import Cart from "./components/Cart";
import Home from "./components/Home";
import { Provider } from "react-redux";
import store from "./redux/store";
import CoffeePage from "./components/CoffeePage";

const theme = createTheme({
  palette: {
    primary: {
      main: "#dde6e7",
    },
    secondary: {
      main: "#222222",
    },
  },
});

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<App />}>
              <Route index element={<Home />} />
              <Route path="/about" element={<About />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/coffeepage:id" element={<CoffeePage/>} />
            </Route>
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </Provider>
  </React.StrictMode>
);
