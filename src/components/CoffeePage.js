import { Avatar, Button, Grid, Typography } from "@mui/material";
import { Container, Stack } from "@mui/system";
import React from "react";
import { useParams } from "react-router-dom";
import { getCoffee } from "../config/data";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { useDispatch, useSelector } from "react-redux";
import {
  decCoffeeCount,
  incCoffeeCount,
} from "../redux/coffeepage/coffeeAction";
import {
  addToCart,
  decBadge,
  deductBill,
  incBadge,
  removeFromCart,
  updateBill,
} from "../redux/cart/cartActions";

const CoffeePage = () => {
  const coffeeCount = useSelector((state) => state.coffee.count);
  console.log(coffeeCount);
  const dispatch = useDispatch();
  const { id } = useParams();
  const _id = id.split(":").join("");
  const data = getCoffee(parseInt(_id));
  const element = () => {
    let ans;
    coffeeCount.forEach((coffee) => {
      if (coffee._id === parseInt(_id)) {
        ans = coffee.cnt;
      }
    });
    return ans;
  };
  return (
    <Container sx={{ py: 12 }} maxWidth="lg">
      <Grid container>
        <Grid item xs={12} md={12} lg={12}></Grid>
        <Grid item xs={12} md={6} lg={4} sx={{ p: 5 }}>
          <Avatar
            src={data.img}
            sx={{ width: "250px", height: "250px", border: "2px solid black" }}
          />
        </Grid>
        <Grid item xs={12} md={6} lg={8} sx={{ p: 5 }}>
          <Typography variant="h2" align="left">
            {data.name}
          </Typography>
          <Typography variant="body1" sx={{ fontSize: "25px" }}>
            {data.desc}
          </Typography>
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
          lg={12}
          sx={{
            display: "flex",
            justifyContent: "flex-end",
            alignItems: "center",
          }}
        >
          <Stack
            direction="row"
            spacing={2}
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "center",
            }}
          >
            <Button
              sx={{
                background: "black",
                "&:hover": {
                  background: "teal",
                },
              }}
              onClick={() => {
                dispatch(incCoffeeCount(parseInt(_id)));
                dispatch(addToCart(data));
                dispatch(updateBill(data.cost));
                dispatch(incBadge());
              }}
            >
              <AddIcon fontSize="large" />
            </Button>
            <Typography variant="h5">{element()}</Typography>
            <Button
              sx={{
                backgroundColor: "black",
                "&:hover": {
                  background: "teal",
                },
              }}
              onClick={() => {
                dispatch(decCoffeeCount(parseInt(_id)));
                dispatch(removeFromCart(data));
                dispatch(deductBill(data.cost));
                dispatch(decBadge());
              }}
            >
              <RemoveIcon fontSize="large" />
            </Button>
          </Stack>
        </Grid>
      </Grid>
    </Container>
  );
};

export default CoffeePage;
