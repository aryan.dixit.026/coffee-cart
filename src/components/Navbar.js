import {
  AppBar,
  ListItemIcon,
  Toolbar,
  Typography,
  Link as LinkL,
  Box,
  Container,
} from "@mui/material";
import CoffeeIcon from "@mui/icons-material/Coffee";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import Badge from '@mui/material/Badge';
import { styled } from '@mui/material/styles';
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";


const StyledBadge = styled(Badge)(({ theme }) => ({
  '& .MuiBadge-badge': {
    right: -3,
    top: 13,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: '0 4px',
  },
}))

const Navbar = (props) => {
  const navigate = useNavigate();
  const badgeVal = useSelector(state=>state.cart.badge)
  return (
    <AppBar color="secondary" position="static">
      <Container maxWidth="lg">
        <Toolbar sx={{ display: "flex", justifyContent: "space-between" }}>
          <Typography
            variant="h6"
            color="primary"
            sx={{
              width: { lg: "27%", md: "50%", sm: "60%" },
              display: "flex",
              justifyContent: "space-evenly",
              cursor: "pointer",
            }}
          >
            <ListItemIcon
              onClick={() => {
                navigate("/");
              }}
            >
              <CoffeeIcon fontSize="large" color="primary" />
            </ListItemIcon>
            <LinkL
              underline="none"
              onClick={() => {
                navigate("/");
              }}
            >
              Home
            </LinkL>
            <LinkL underline="none" onClick={() => navigate("/about")}>
              About
            </LinkL>
          </Typography>
          <Box
            style={{
              width: "35%",
              textAlign: "right",
            }}
          >
            <ListItemIcon
              onClick={() => {
                navigate("/cart");
              }}
              sx={{ cursor: "pointer" }}
            >
              <StyledBadge badgeContent={badgeVal} color="secondary">
                <ShoppingCartIcon fontSize="large" color="primary" />
              </StyledBadge>
            </ListItemIcon>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Navbar;
