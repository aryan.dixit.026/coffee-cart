import { Typography } from "@mui/material";
import { Container } from "@mui/system";
import React from "react";
// import { useEffect } from "react";
import { useSelector } from "react-redux";
import CartItem from "./CartItem";

const Cart = () => {
  const cartArray = useSelector((state) => state.cart.cartArray);
  const bill = useSelector((state) => state.cart.bill);

  const data = cartArray.map((item) => (
    <CartItem coffee={item} key={item.id} />
  ));

  return (
    <Container maxWidth="lg" sx={{ py: 8, my: 8, textAlign: "center" }}>
      <Typography variant="h3">CART</Typography>
      {data}
      {cartArray.length !== 0 && (
        <Typography variant="h4" align="right" sx={{ px: 30, py: 5 }}>
          Total Bill : {bill} RS/-
        </Typography>
      )}
      {cartArray.length === 0 && (
        <Typography variant="h4">SORRY CART IS EMPTY</Typography>
      )}
    </Container>
  );
};

export default Cart;
