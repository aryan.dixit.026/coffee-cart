import React, { useState } from "react";
import { Button, Grid, List, Stack } from "@mui/material";
import { Container } from "@mui/system";
import CoffeeCard from "../components/CoffeeCard";
import coffeeData from "../config/data";
import CoffeeListCard from "./CoffeeListCard";
import ListIcon from "@mui/icons-material/List";
import GridViewIcon from "@mui/icons-material/GridView";

const Home = () => {
  const [view, setView] = useState(false);
  const data = coffeeData.map((cup) => (
    <CoffeeCard key={cup.id} coffee={cup}  />
  ));
  const dataList = coffeeData.map((cup) => (
    <CoffeeListCard key={cup.id} coffee={cup} />
  ));
  return (
    <>
      <Container sx={{ py: 8}} maxWidth="md">
        <Container maxWidth="sm">
          <Stack
            direction="row"
            spacing={2}
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "stretch",
            }}
          >
            <Button
              sx={{
                background: "black",
                "&:hover": {
                  background: "teal",
                },
              }}
              onClick={() => setView(true)}
            >
              <GridViewIcon fontSize="large" />
            </Button>
            <Button
              sx={{
                backgroundColor: "black",
                "&:hover": {
                  background: "teal",
                },
              }}
              onClick={() => setView(false)}
            >
              <ListIcon fontSize="large" />
            </Button>
          </Stack>
        </Container>
        {view ? (
          <Grid container spacing={4} sx={{ marginTop: "5px" }} maxWidth="md">
            {data}
          </Grid>
        ) : (
          <List sx={{ width: "100%" }}>{dataList}</List>
        )}
      </Container>
    </>
  );
};

export default Home;
