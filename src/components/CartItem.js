import {
  Avatar,
  Divider,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import RemoveIcon from "@mui/icons-material/Remove";
import React from "react";
import { useDispatch } from "react-redux";
import { decBadge, deductBill, removeFromCart } from "../redux/cart/cartActions";
import { decCoffeeCount } from "../redux/coffeepage/coffeeAction";

const CartItem = (props) => {
  const dispatch = useDispatch();
  return (
    <>
      <ListItem sx={{ width: "100%", display: "flex", mY: 2 }}>
        <ListItemAvatar>
          <Avatar
            src={props.coffee.img}
            sx={{ width: "100px", height: "100px" }}
          />
        </ListItemAvatar>
        <ListItemText
          primary={
            <Typography variant="h4">
              {props.coffee.name.toUpperCase()}
            </Typography>
          }
          sx={{
            width: "20px",
            display: "flex",
            justifyContent: "center",
            py: 2,
          }}
        />
        <ListItemText
          sx={{
            p: 2,
            textAlign: "center",
          }}
          primary={
            <Typography variant="h5">{props.coffee.cost} RS/-</Typography>
          }
        />
        <IconButton
          sx={{
            border: "2px solid black",
            "&:hover": { backgroundColor: "black", color: "white" },
            backgroundColor: "white",
            color: "black",
          }}
          onClick={() => {
            dispatch(removeFromCart(props.coffee));
            dispatch(deductBill(props.coffee.cost));
            dispatch(decCoffeeCount(props.coffee.id));
            dispatch(decBadge())
          }}
        >
          <RemoveIcon
       
          />
        </IconButton>
      </ListItem>
      <Divider
        sx={{
          border: "1px solid black",
          width: "100%",
          backgroundColor: "black",
        }}
      />
    </>
  );
};

export default CartItem;
