import ShoppingCart from "@mui/icons-material/ShoppingCart";
import {
  Avatar,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemIcon,
  ListItemText,
  Typography,
} from "@mui/material";
import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { addToCart, incBadge, updateBill } from "../redux/cart/cartActions";
import { incCoffeeCount } from "../redux/coffeepage/coffeeAction";
const CoffeeListCard = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  return (
    <ListItem
      sx={{
        border: "2px solid black",
        width: "100%",
        display: "flex",
        m: 2,
        "&:hover": { cursor: "pointer" },
      }}
    >
      <ListItemAvatar
        onClick={() => {
          navigate(`/coffeepage:${props.coffee.id}`);
        }}
      >
        <Avatar
          src={props.coffee.img}
          sx={{ width: "100px", height: "100px" }}
        />
      </ListItemAvatar>
      <ListItemText
        primary={
          <Typography variant="h4">
            {props.coffee.name.toUpperCase()}
          </Typography>
        }
        secondary={<Typography variant="body2">{props.coffee.desc}</Typography>}
        sx={{ maxWidth: "65%", p: 2 }}
        onClick={() => {
          navigate(`/coffeepage:${props.coffee.id}`);
        }}
      />
      <ListItemText
        sx={{ p: 2, textAlign: "center" }}
        primary={<Typography variant="h5">{props.coffee.cost} RS/-</Typography>}
      />
      <ListItemIcon>
        <IconButton
         onClick={() => {
          dispatch(addToCart(props.coffee));
          dispatch(updateBill(props.coffee.cost));
          dispatch(incCoffeeCount(props.coffee.id));
          dispatch(incBadge());
        }}
        >
          <ShoppingCart
            color="secondary"
           
          />
        </IconButton>
      </ListItemIcon>
    </ListItem>
  );
};

export default CoffeeListCard;
