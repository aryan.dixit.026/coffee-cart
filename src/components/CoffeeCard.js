import ShoppingCart from "@mui/icons-material/ShoppingCart";
import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  IconButton,
  Typography,
} from "@mui/material";
import { red } from "@mui/material/colors";
import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { addToCart, incBadge, updateBill } from "../redux/cart/cartActions";
import { incCoffeeCount } from "../redux/coffeepage/coffeeAction";

const CoffeeCard = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const date = new Date();
  return (
    <Grid
      item
      xs={12}
      sm={6}
      md={4}
      sx={{ display: "flex", "&:hover": { cursor: "pointer" } }}
     
    >
      <Card
        sx={{
          maxWidth: 345,
          border: "2px solid black",
          display: "flex",
          justifyContent: "space-between",
          flexDirection: "column",
        }}
      >
        <CardHeader
          avatar={
            <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
              {props.coffee.name[0]}
            </Avatar>
          }
          title={
            <Typography variant="h6">
              {props.coffee.name.toUpperCase()}
            </Typography>
          }
          subheader={date.toLocaleString()}
          style={{ maxHeight: "80px" }}
          onClick={() => {
            navigate(`/coffeepage:${props.coffee.id}`);
          }}
        />
        <CardMedia
          component="img"
          height="194"
          image={props.coffee.img}
          alt={props.coffee.name}
          onClick={() => {
            navigate(`/coffeepage:${props.coffee.id}`);
          }}
        />

        <CardContent sx={{ flexGrow: 1 }}>
          <Typography variant="body1">{props.coffee.desc}</Typography>
        </CardContent>
        <CardActions
          sx={{
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Typography variant="h6">{props.coffee.cost} RS/-</Typography>
          <IconButton>
            <ShoppingCart
              color="secondary"
              onClick={() => {
                dispatch(addToCart(props.coffee));
                dispatch(updateBill(props.coffee.cost));
                dispatch(incCoffeeCount(props.coffee.id));
                dispatch(incBadge());
              }}
            />
          </IconButton>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default CoffeeCard;
