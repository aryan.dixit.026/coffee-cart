import { Container, Typography } from "@mui/material";
import React from "react";

const About = () => {
  return (
    <>
      <Container sx={{ p: 8 }} maxWidth="lg">
        <Typography variant="h1">ABOUT PAGE</Typography>
      </Container>
    </>
  );
};

export default About;
