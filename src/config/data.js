const coffeeData = [
  {
    id: 1,
    name: "Espresso",
    img: "../images/expresso.jpg",
    desc: "The espresso, also known as a short black, is approximately 1 oz. of highly concentrated coffee. Although simple in appearance, it can be difficult to master.",
    cost: 50,
  },
  {
    id: 2,
    name: "Double Espresso",
    img: "../images/black_eye.jpg",
    desc: "A double espresso may also be listed as doppio, which is the Italian word for double. This drink is highly concentrated and strong.",
    cost: 100,
  },
  {
    id: 3,
    name: "Red Eye",
    img: "../images/double_expresso.jpg",
    desc: "The red eye's purpose is to add a boost of caffeine to your standard cup of coffee",
    cost: 120,
  },
  {
    id: 4,
    name: "Black Eye",
    img: "../images/americano.jpg",
    desc: "The black eye is just the doubled version of the red eye and is very high in caffeine.",
    cost: 100,
  },
  {
    id: 5,
    name: "Americano",
    img: "../images/long_black.jpg",
    desc: "Americanos are popular breakfast drinks and thought to have originated during World War II. Soldiers would add water to their coffee to extend their rations farther. The water dilutes the espresso while still maintaining a high level of caffeine",
    cost: 50,
  },
  {
    id: 6,
    name: "Long Black",
    img: "../images/expresso.jpg",
    desc: "The long black is a similar coffee drink to the americano, but it originated in New Zealand and Australia. It generally has more crema than an americano..",
    cost: 75,
  },
  {
    id: 7,
    name: "Macchiato",
    img: "../images/black_eye.jpg",
    desc: "The word macchiato means mark or stain. This is in reference to the mark that steamed milk leaves on the surface of the espresso as it is dashed into the drink. Flavoring syrups are often added to the drink according to customer preference",
    cost: 250,
  },
  {
    id: 8,
    name: "Long Machhiato",
    img: "../images/double_expresso.jpg",
    desc: "Often confused with a standard macchiato, the long macchiato is a taller version and will usually be identifiable by its distinct layers of coffee and steamed milk.",
    cost: 350,
  },
  {
    id: 9,
    name: "Cortado",
    img: "../images/americano.jpg",
    desc: "The cortado takes the macchiato one step further by evenly balancing the espresso with warm milk in order to reduce the acidity.",
    cost: 150,
  },
  {
    id: 10,
    name: "Breve",
    img: "../images/long_black.jpg",
    desc: "The breve provides a decadent twist on the average espresso, adding steamed half-and-half to create a rich and creamy texture.",
    cost: 100,
  },
  {
    id: 11,
    name: "Cappuccino",
    img: "../images/expresso.jpg",
    desc: "This creamy coffee drink is usually consumed at breakfast time in Italy and is loved in the United States as well. It is usually associated with indulgence and comfort because of its thick foam layer and additional flavorings that can be added to it",
    cost: 175,
  },
  {
    id: 12,
    name: "Flat White",
    img: "../images/red_eye.jpg",
    desc: "A flat white also originates from New Zealand and Australia and is very similar to a cappuccino but lacks the foam layer and chocolate powder. To keep the drink creamy rather than frothy, steamed milk from the bottom of the jug is used instead of from the top",
    cost: 200,
  },
  {
    id: 13,
    name: "Cafe Latte",
    img: "../images/black_eye.jpg",
    desc: "Cafe lattes are considered an introductory coffee drink since the acidity and bitterness of coffee are cut by the amount of milk in the beverage. Flavoring syrups are often added to the latte for those who enjoy sweeter drinks.",
    cost: 225,
  },
  {
    id: 14,
    name: "Mocha",
    img: "../images/double_expresso.jpg",
    desc: "The mocha is considered a coffee and hot chocolate hybrid. The chocolate powder or syrup gives it a rich and creamy flavor and cuts the acidity of the espresso.",
    cost: 250,
  },
  {
    id: 15,
    name: "Vienna",
    img: "../images/americano.jpg",
    desc: "There are a few variations on the Vienna, but one of the most common is made with two ingredients: espresso and whipped cream. The whipped cream takes the place of milk and sugar to provide a creamy texture.",
    cost: 75,
  },
  {
    id: 16,
    name: "Affogato",
    img: "../images/long_black.jpg",
    desc: "Affogatos are more for a dessert coffee than a drink you would find at a cafe, but they can add a fun twist to your coffee menu. They are made by pouring a shot of espresso over a scoop of vanilla ice cream to create a sweet after-meal treat.",
    cost: 125,
  },
  {
    id: 17,
    name: "Cafe au Lait",
    img: "../images/expresso.jpg",
    desc: "The cafe au lait is typically made with French press coffee instead of an espresso shot to bring out the different flavors in the coffee. It is then paired with scalded milk instead of steamed milk and poured at a 50/50 ratio.",
    cost: 150,
  },
  {
    id: 18,
    name: "Iced Coffee",
    img: "../images/red_eye.jpg",
    desc: "Iced coffees become very popular in the summertime in the United States. The recipes do have some variance, with some locations choosing to interchange milk with water in the recipe. Often, different flavoring syrups will be added per the preference of the customer.",
    cost: 200,
  },
];

export default coffeeData;

export function getCoffee(num) {
    let val ;
    coffeeData.forEach(element => {
        if(element.id === num){
            val = element;
        }  
    });
    return val;
}
