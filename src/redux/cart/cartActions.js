import { ADD_TO_CART, DEC_BADGE, DEDUCT_BILL, INC_BADGE, REMOVE_FROM_CART, UPDATE_BILL } from "./cartTypes"

const addToCart = (ele)=>{
    return {
        type : ADD_TO_CART,
        payload : ele
    }
}

const updateBill = (cost) =>{
    return {
        type : UPDATE_BILL,
        payload : cost
    }
}

const deductBill = (cost) =>{
    return {
        type : DEDUCT_BILL,
        payload : cost
    }
}

const removeFromCart = (ele)=>{
    return {
        type : REMOVE_FROM_CART,
        payload : ele
    }
}

const incBadge = () => {
    return {
        type : INC_BADGE
    }
}

const decBadge = () => {
    return {
        type : DEC_BADGE
    }
}


export {
    addToCart,
    updateBill,
    removeFromCart,
    deductBill,
    incBadge,
    decBadge
}