import { ADD_TO_CART, DEC_BADGE, DEDUCT_BILL, INC_BADGE, REMOVE_FROM_CART, UPDATE_BILL } from "./cartTypes"
import removeElement from "./help"

const initialState = {
    badge : 0,
    bill : 0,
    cartArray : []
}


const cartReducer = (state = initialState, action)=>{
    switch(action.type){
        case ADD_TO_CART : return {
            ...state,
            cartArray : [...state.cartArray, action.payload]
        }

        case UPDATE_BILL : return {
            ...state,
            bill : state.bill + action.payload
        }

        case REMOVE_FROM_CART : return {
            ...state,
            cartArray : removeElement(state.cartArray, action.payload)
        }

        case DEDUCT_BILL : return {
            ...state,
            bill : state.bill - action.payload
        }

        case INC_BADGE : return {
            ...state,
            badge : state.badge + 1
        }
        case DEC_BADGE : return {
            ...state,
            badge : state.badge - 1
        }
        default : return state
    }    
}

export default cartReducer