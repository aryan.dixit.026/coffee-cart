import { combineReducers } from "redux";
import coffeeReducer from "./coffeepage/coffeeReducer";
import cartReducer from "./cart/cartReducer";

const rootReducer = combineReducers({
    coffee : coffeeReducer,
    cart : cartReducer
})

export default rootReducer