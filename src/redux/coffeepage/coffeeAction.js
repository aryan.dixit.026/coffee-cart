import { DEC_COUNT, INC_COUNT } from "./coffeeTypes"

const incCoffeeCount = (_id)=>{
    return {
        type : INC_COUNT,
        payload : _id
    }
}

const decCoffeeCount = (_id)=>{
    return {
        type : DEC_COUNT,
        payload : _id
    }
}

export {
    incCoffeeCount,
    decCoffeeCount
}

