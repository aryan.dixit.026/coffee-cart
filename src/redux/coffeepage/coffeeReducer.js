import coffeeData from "../../config/data";
import { DEC_COUNT, INC_COUNT } from "./coffeeTypes";


const initialState = {
  count: coffeeData.map(coffee=>({_id : coffee.id, cnt : 0})),
};

const coffeeReducer = (state = initialState, action) => {
  switch (action.type) {
    case INC_COUNT:
      return {
        ...state,
        count:state.count.map(ele=>{
            if(ele._id === action.payload){
                ele.cnt = ele.cnt + 1
            }
            return ele
        }) ,
      };
    case DEC_COUNT:
      return {
        ...state,
        count:state.count.map(ele=>{
            if(ele._id === action.payload){
                ele.cnt = ele.cnt - 1
            }
            return ele
        })
      };
    default:
      return state;
  }
};

export default coffeeReducer;
